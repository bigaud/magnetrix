﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PlayerPlayState : PlayerStateBase
{
    /// <summary>
    /// Game Manager is the script that makes change level
    /// 
    /// the world we play is currentworld and the level current
    /// </summary>
    /// 
    public override void OnEnter(PlayerBehavior player)
    {
        // We activate the line renderer
        player.lineRenderer.enabled = true;
        player.PlayerZone.color = new Color32(255, 255, 255, 255);
        // We update the ready button 
        player.ath.ClearReadyButtonEvents();
        player.ath.SetReadyButtonText("END TURN");
        // We create the new event to add to the button
        UnityEvent evt = new UnityEvent();
        evt.AddListener(player.EndTurn);
        player.ath.SetReadyButtonEvents(evt);
        player.ath.MoveButton(true);
        NfcAsync.StartTagDetection();
        if (player.playerPos.localPosition != Vector3.zero)
        {
            player.lineRenderer.gameObject.SetActive(true);
        }
        if (player.pionplacer)
        {
            player.circle.SetActive(true);
        }
    }


    /// <summary>
    /// if player is play 
    /// </summary>
    /// <param name="player"></param>
   public override void Update(PlayerBehavior player)
   {
      // if the Player did not play
        if (GameManager.Instance.State== GameStateBase.GAME_STATE_DRAW && player.countCard>0)
      {
         player.countCard = 0;
         player.cardsPlayed.Clear();
         
      }
      else if(GameManager.Instance.State == GameStateBase.GAME_STATE_PLAY)
      {
            //Reduce Cooldown by Time
            if (player.coolDownRemaining >= 0)
            {
                player.coolDownRemaining -= Time.deltaTime;
                player.ath.cooldown.fillAmount = 1-(player.coolDownRemaining / player.cooldownTime);
            }
            player.OnTouchScreen();
        }
        
      // If we click on button, we do nothing to avoid change position
        if (EventSystem.current.currentSelectedGameObject != null)
         return;
        
      //If Click
      if (Input.GetMouseButtonDown(0))
      {
         player.DetectZoneTouch(Input.mousePosition, 0);
      }
            
      //If Touch
      if (Input.touchCount > 0)
      {
         foreach (Touch touch in Input.touches)
         {
            player.DetectZoneTouch(touch.position, touch.fingerId);
         }     
      }

       /* if (player.force>=30)
        {
            player.ath
        }*/
    }

   public override void OnExit(PlayerBehavior player)
   {
      // We activate the line renderer
      player.lineRenderer.enabled = false;
      // We clear the interface to prepare the new draw phase
      player.ath.ClearReadyButtonEvents();
      player.PlayerZone.color = new Color32(94, 94, 94, 255);
      player.ath.SetReadyButtonColor(new Color32(80,80,80,64));
   }
}
