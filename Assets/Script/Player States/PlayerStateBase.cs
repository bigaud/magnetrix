﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerStateBase : IPlayerState
{
    /// <summary>
    /// this is the abstract class for state player
    /// </summary>
    /// 

    // Some getters for all play state
    public static readonly PlayerDrawState PLAYER_STATE_DRAW = new PlayerDrawState();
    public static readonly PlayerWaitState PLAYER_STATE_WAIT = new PlayerWaitState();
    public static readonly PlayerPlayState PLAYER_STATE_PLAY = new PlayerPlayState();
    public virtual void OnEnter(PlayerBehavior player){}
    public virtual void OnExit(PlayerBehavior player){}

    public virtual void ToState(PlayerBehavior player, IPlayerState targetState)
    {
        // as explain in interface the state exit and is passed to another who begin with OnEnter
        player.State.OnExit(player);
        player.State = targetState;
        player.State.OnEnter(player);
    }

    public virtual void Update(PlayerBehavior player) { }
}
