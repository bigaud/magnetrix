﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerState
{
    /// <summary>
    /// The interface is used to create Playerstate abstract class, it is based on construct pattern script and have 4 method
    /// On enter (begin state in to state)
    /// To state (transition between state) go to one state to the next
    /// On Exit, begin in to state end mark the end of the previous state
    /// Update, is played in a update loop, and function the same.
    /// 
    /// attach to each player
    /// </summary>
    /// <param name="player"></param>
    void OnEnter(PlayerBehavior player);
    void OnExit(PlayerBehavior player);
    void ToState(PlayerBehavior player, IPlayerState targetState);
    void Update(PlayerBehavior player);
}
