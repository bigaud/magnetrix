﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWaitState : PlayerStateBase
{
    /// <summary>
    /// state when the player is not used before draw
    /// </summary>
    /// 

    public override void OnEnter(PlayerBehavior player)
    {
        base.OnEnter(player);
        player.ath.ActiveIndicationCarte(false);
        player.ath.SetIncitationButton(false);
        player.circle.SetActive(false);
    }

    public override void OnExit(PlayerBehavior player)
    {
        // We clear the interface to prepare the new draw phase
        player.ath.ClearReadyButtonEvents();
    }
}
