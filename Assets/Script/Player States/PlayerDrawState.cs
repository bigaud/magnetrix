﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerDrawState : PlayerStateBase
{
    /// <summary>
    /// when the player plays
    /// </summary>
    /// <param name="player"></param>

    public override void OnEnter(PlayerBehavior player)
    {
        player.ath.ClearReadyButtonEvents();
        
        player.ath.SetReadyButtonText("READY");

        UnityEvent evt = new UnityEvent();
        evt.AddListener(player.SetReady);
        player.ath.SetReadyButtonEvents(evt);
        player.ath.SetReadyButtonColor(new Color32(255, 255, 255, 255));
        player.ath.MoveButton(false);
    }


}
