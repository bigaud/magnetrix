﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "New World Data")]
public class WorldData : ScriptableObject
{
    public List<string> levelNames;
}

public struct LevelData
{
    public string sceneName;
    public Scene scene;
    public GameObject root;

    public LevelData(string name, Scene scene, GameObject root)
    {
        sceneName = name;
        this.scene = scene;
        this.root = root;
    }
}