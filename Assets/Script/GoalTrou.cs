﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalTrou : MonoBehaviour
{
    /// <summary>
    /// This script is used for the trouTP
    /// 
    /// </summary>
    /// 

    public ParticleSystem goal;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag=="Ball")
        {
            Debug.Log("emit object");
            goal.Play();
        }
    }
}
