﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A class base for the game state
public abstract class GameStateBase : IGameState
{
    // Some getters for all game state
    public static readonly IGameState GAME_STATE_DRAW = new GameDrawState();
    public static readonly IGameState GAME_STATE_PLAY = new GamePlayState();
    public static readonly IGameState GAME_STATE_VICTORY = new GameVictoryState();
    public static readonly IGameState GAME_STATE_WAIT = new GameWaitState();
    public virtual void OnEnter(GameManager gameManager) { }

    public virtual void OnExit(GameManager gameManager) { }

    public virtual void ToState(GameManager gameManager, IGameState targetState)
    {
        // as explain in interface the state exit and is passed to another who begin with OnEnter
        gameManager.State.OnExit(gameManager);
        gameManager.State = targetState;
        gameManager.State.OnEnter(gameManager);
    }

    public virtual void Update(GameManager gameManager) {}
}
