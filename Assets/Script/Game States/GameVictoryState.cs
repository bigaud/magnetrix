﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameVictoryState : GameStateBase
{
    /// <summary>
    /// Draw is a state of pause where the player play their carte
    /// </summary>
    /// <param name="gameManager"></param>

    public override void OnEnter(GameManager gameManager)
    {
        // all player to wait
        foreach (PlayerBehavior player in gameManager.players)
        {
            player.State.ToState(player, PlayerStateBase.PLAYER_STATE_WAIT);
            player.ath.ClearReadyButtonEvents();
        }
        //gameManager.athManager.victoryParticle.Play();
        gameManager.athManager.ShowVictoryStatePanel(true);
        gameManager.athManager.UpdateVictoryStateFields(gameManager.scoreTeamRed, gameManager.scoreTeamBlue);
        //Play Victory jingle
        AudioManager.instance.Stop("Mus_Laby");
        AudioManager.instance.Stop("Mus_Laby_Deep");
        AudioManager.instance.Stop("Mus_Pachi");
        AudioManager.instance.Stop("Mus_Pachi_Deep");
        AudioManager.instance.Play("Mus_Victory");
    }

    //state who provide info for the game
    public override void OnExit(GameManager gameManager)
    {
    }
}
