﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameState 
{
    /// <summary>
    /// The interface is used to create GameState abstract class, it is based on construct pattern script and have 4 method
    /// On enter (begin state in to state)
    /// To state (transition between state) go to one state to the next
    /// On Exit, begin in to state end mark the end of the previous state
    /// Update, is played in a update loop, and function the same.
    /// </summary>
    /// <param name="gameMananager"></param>
    void OnEnter(GameManager gameMananager);
    void OnExit(GameManager gameMananager);
    void ToState(GameManager gameMananager, IGameState targetState);
    void Update(GameManager gameMananager);
}
