﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A class base for the game state
public class GameWaitState : GameStateBase
{
    /// <summary>
    /// this scri
    /// </summary>
    /// <param name="gameManager"></param>
    public override void OnEnter(GameManager gameManager)
    {
        gameManager.athManager.ActivateObject(gameManager.athManager.gainPointVFX.gameObject, true);
        gameManager.athManager.SetAnimationInfo(false, false);
        gameManager.athManager.ShowPlayersStatePanel(true);
        gameManager.athManager.ActivePanelInfoTeamPoint(true);
        foreach (PlayerBehavior player in gameManager.players)
        {
            player.State.ToState(player, PlayerStateBase.PLAYER_STATE_WAIT);
            gameManager.athManager.ActivePlayerStateFields(player, false);
        }
        gameManager.Invoke("OnLevelEnded", gameManager.timeWaitState);

        // ici on mets le vfx

        // effet de score
        gameManager.athManager.Invoke("UpdateScoreFields", gameManager.timeUpdateScore);
    }

    public override void OnExit(GameManager gameManager)
    {
        //gameManager.athManager.ActivePanelInfoTeamPoint(false);
        gameManager.athManager.ActivateObject(gameManager.athManager.gainPointVFX.gameObject, false);
    }
}
