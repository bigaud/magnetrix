﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayState : GameStateBase
{
    /// <summary>
    /// GameplayState come from abstract class GameStateBase
    /// he change all state player to play state
    /// </summary>
    /// <param name="gameManager"></param>
    public override void OnEnter(GameManager gameManager)
    {
        foreach (PlayerBehavior player in gameManager.players)
        {
            player.State.ToState(player, PlayerStateBase.PLAYER_STATE_PLAY);
            Debug.Log(gameManager.isLevelBeginning);
            if (gameManager.isLevelBeginning==true)
            {
                player.ath.pionIncitation.gameObject.SetActive(true);
            }
        }
        if (gameManager.isLevelBeginning)
        {
            Debug.Log("passeparislevelb");
            AudioManager.instance.Play("MancheStartLaby");
            gameManager.isLevelBeginning = false;
        }
        else
        {
            AudioManager.instance.Play("Reprise");
        }
        //Change the music
        AudioManager.instance.ChangeVolume("Mus_Laby", 0.85f);
        AudioManager.instance.ChangeVolume("Mus_Laby_Deep", 0);
        AudioManager.instance.ChangeVolume("Mus_Pachi", 0.85f);
        AudioManager.instance.ChangeVolume("Mus_Pachi_Deep", 0);
    }
}
