﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDrawState : GameStateBase
{
    /// <summary>
    /// Draw is a state of pause where the player play their carte
    /// </summary>
    /// <param name="gameManager"></param>
    public override void OnEnter(GameManager gameManager)
    {
        //state who provide info for the game
        gameManager.athManager.ShowPlayersStatePanel(true);
        // We init the interface to set all players not ready to state draw
        foreach (PlayerBehavior player in gameManager.players)
        {
            // change to draw / zone color
            player.countCard = 0;
            player.State.ToState(player, PlayerStateBase.PLAYER_STATE_DRAW);
            player.PlayerZone.color = new Color32(94, 94, 94, 255);
            // change color of the state fields
            gameManager.athManager.ActivePlayerStateFields(player, true);
            gameManager.athManager.UpdatePlayerStateFields(player, "NOT READY", Color.red);
        }

        if (gameManager.countTurn==0 || gameManager.countTurn == 2)
        {
            foreach (PlayerBehavior player in gameManager.players)
            {
                player.ath.ClearCardCount();
            }
            gameManager.athManager.UpdatePanelInfoTeamPoint("shuffle and draw 4");
            gameManager.athManager.SetAnimationInfo(true,true);
        }
        else
        {
            gameManager.athManager.UpdatePanelInfoTeamPoint("refill up to 4");
            gameManager.athManager.SetAnimationInfo(true, false);
        }

        // change actual music
        AudioManager.instance.ChangeVolume("Mus_Laby", 0);
        AudioManager.instance.ChangeVolume("Mus_Laby_Deep", 0.85f);
        AudioManager.instance.ChangeVolume("Mus_Pachi", 0);
        AudioManager.instance.ChangeVolume("Mus_Pachi_Deep", 0.85f);


        // when numberturn is superior to 2 default or more 
        if (gameManager.countTurn>= gameManager.turnChange)
        {
            gameManager.ResetPlayerCards();
        }
        gameManager.countTurn++;
        AudioManager.instance.Play("Pause");
    }

    //state who provide info for the game
    public override void OnExit(GameManager gameManager)
    {
        gameManager.athManager.ShowPlayersStatePanel(false);
        /// we will add here an element to see if the game manager reload the player card.
    }
}
