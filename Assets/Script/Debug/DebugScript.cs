﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DebugScript : MonoBehaviour
{
    public Text console;

    private GlobalVariables gVar;
    private TouchScreenKeyboard keyboard;

    private void Start()
    {
        gVar = FindObjectOfType<GlobalVariables>();
    }

    //Enable Virtual Keyboard
    private void OnEnable()
    {
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
    }

    //Restart Scene
    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    //Reset console color
    public void TypeCommand(Text input)
    {
        input.color = Color.black;
    }

    public void EnterCommand(Text input)
    {
        //Get command
        string command = input.text;
        string[] array = command.Split(' ');

        switch (array[0])
        {
            //Change global variable
            case "set":
                if (array.Length == 3 && gVar.GetType().GetField(array[1]).GetValue(gVar) != null)
                {
                    console.text += "\n" + "Set value " + array[1] + " to " + array[2];
                    gVar.GetType().GetField(array[1]).SetValue(gVar, float.Parse(array[2]));
                    input.GetComponentInParent<InputField>().text = "";
                }
                else
                {
                    //ERROR
                    console.text += "\n" + "ERROR Syntaxe : 'set [variable] [value]'";
                    input.color = Color.red;
                }
                break;

            case "level":
                
                break;

            case "z":
                break;

            case "e":
                break;

            case "r":
                break;

            default:
                //ERROR
                console.text += "\n" + "ERROR Unknown Command";
                input.color = Color.red;
                break;
        }

        gVar.ResetVariables();
    }
}