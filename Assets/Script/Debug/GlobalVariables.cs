﻿using UnityEngine;

public class GlobalVariables : MonoBehaviour
{
    public static GlobalVariables gVar { private set; get; }

    //Ball Variables
    public float b_size = .2f;
    public float b_mass = .4f;
    public float b_drag = 0;
    public float b_angularDrag = 0;
    public float b_dynamicFriction = .05f;
    public float b_staticFriction = 0;
    public float b_bounciness = .6f;
    public float g_VolumeGeneral;
    public float coolDownPlayer=1;
    public int maxForce = 30;
    public int currentWorldIndex = 0;

    //Refs
    BallBehavior ball;

    //world
    public int world=1;

    //Keep one Instance
    void Awake()
    {
        if (gVar == null)
        {
            gVar = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject); // or gameObject

        ResetVariables();
        gVar = this;
    }

    public void ResetVariables()
    {
        //Find ball
        ball = FindObjectOfType<BallBehavior>();
        if (ball!=null)
        {
            //Reset all variables
            ball.transform.localScale = new Vector3(b_size, b_size, b_size);
            var b_rig = ball.GetComponent<Rigidbody>();
            b_rig.mass = b_mass;
            b_rig.drag = b_drag;
            b_rig.angularDrag = b_angularDrag;
            var b_coll = ball.GetComponent<SphereCollider>();
            b_coll.material.dynamicFriction = b_dynamicFriction;
            b_coll.material.staticFriction = b_staticFriction;
            b_coll.material.bounciness = b_bounciness;
        }
        
    }
}
