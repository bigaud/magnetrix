﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmulCards : MonoBehaviour
{
    public enum CardEnum { As, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King };

    public List<CardEnum> cards = new List<CardEnum> {
        CardEnum.As,
        CardEnum.Two,
        CardEnum.Three,
        CardEnum.Four,
        CardEnum.Five,
        CardEnum.Six,
        CardEnum.Seven,
        CardEnum.Eight,
        CardEnum.Nine,
        CardEnum.Ten,
        CardEnum.Jack,
        CardEnum.Queen,
        CardEnum.King
    };

    public Button[] hand;

    public Image black;

    [HideInInspector]
    public int nbCard;

    private bool start = true;

    void Update()
    {
        if (Input.touches.Length == 4 && start)
        {
            StartCoroutine(NewHand());
            start = false;
        }

        if (Input.GetKeyDown(KeyCode.Q))
            StartCoroutine(NewHand());
            //NewHand();
    }



    IEnumerator NewHand()
    {
        if (black != null)
        {
            black.enabled = true;
            Time.timeScale = 0f;
            yield return new WaitForSecondsRealtime(3f);
            Time.timeScale = 1f;
            black.enabled = false;
        }


        nbCard = 4;

        foreach (Button card in hand)
        {
            if (cards.Count == 0)
                cards = new List<CardEnum> {
                CardEnum.As,
                CardEnum.Two,
                CardEnum.Three,
                CardEnum.Four,
                CardEnum.Five,
                CardEnum.Six,
                CardEnum.Seven,
                CardEnum.Eight,
                CardEnum.Nine,
                CardEnum.Ten,
                CardEnum.Jack,
                CardEnum.Queen,
                CardEnum.King
            };

            int rnd = Random.Range(0, cards.Count);
            card.onClick.RemoveAllListeners();

           /* switch (cards[rnd])
            {
                case CardEnum.As:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(1, "1"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "1";
                    break;
                case CardEnum.Two:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(2, "2"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "2";
                    break;
                case CardEnum.Three:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(3, "3"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "3";
                    break;
                case CardEnum.Four:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(4, "4"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "4";
                    break;
                case CardEnum.Five:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(5, "5"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "5";
                    break;
                case CardEnum.Six:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(6, "6"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "6";
                    break;
                case CardEnum.Seven:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(7, "7"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "7";
                    break;
                case CardEnum.Eight:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(8, "8"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "8";
                    break;
                case CardEnum.Nine:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(9, "9"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "9";
                    break;
                case CardEnum.Ten:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().AddForce(10, ":"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "10";
                    break;
                case CardEnum.Jack:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().Effect("j"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "J";
                    break;
                case CardEnum.Queen:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().Effect("q"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "Q";
                    break;
                case CardEnum.King:
                    card.onClick.AddListener(() => { GetComponent<PlayerBehavior>().Effect("r"); CardPlayed(); });
                    card.GetComponentInChildren<Text>().text = "K";
                    break;
                default : 
                    break;
            }*/

            cards.RemoveAt(rnd);
            card.interactable = true;
        }
    }

    public void CardPlayed()
    {
        nbCard--;

        int totalCard = 0;
        foreach (EmulCards ec in FindObjectsOfType<EmulCards>())
        {
            totalCard += ec.nbCard;
        }

        if (totalCard == 0)
        {
            foreach (EmulCards ec in FindObjectsOfType<EmulCards>())
            {
                StartCoroutine(ec.NewHand());
            }
        }
    }

    public void Disable(Button self)
    {
        self.interactable = false;
    }
}
