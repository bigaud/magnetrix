﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CardData : ScriptableObject
{
    public string cardName;
    public bool isAnEffect;

    public abstract void ApplyEffect(PlayerBehavior playerBehavior);
}
