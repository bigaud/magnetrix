﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Effect Card Data")]
public class EffectCardData : CardData
{
    public override void ApplyEffect(PlayerBehavior playerBehavior)
    {
        playerBehavior.Effect();
    }
}
