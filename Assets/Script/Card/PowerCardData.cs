﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Power Card Data")]
public class PowerCardData : CardData
{
    public int power;

    public override void ApplyEffect(PlayerBehavior playerBehavior)
    {
        playerBehavior.AddForce(power);
    }
}
