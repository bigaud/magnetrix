﻿using UnityEngine;

public class BallBehavior : MonoBehaviour
{
    /// <summary>
    /// The script ball, mainly sound management
    /// </summary>
    /// 
    public static BallBehavior Instance { private set; get; }

    private float speed;
    private Rigidbody rigidbody;
    public ParticleSystem shockwave;     //To set in Unity
    public float ImmobilisationTimer;

    private void Start()
    {
        AudioManager.instance.Play("BilleRoule");
        Instance = this;

        rigidbody = GetComponent<Rigidbody>();
    }

    // change sound when roll
    private void Update()
    {
        AudioManager.instance.ChangeVolume("BilleRoule", GetComponent<Rigidbody>().velocity.magnitude - .5f);
        if (transform.position.y<-3)
        {
            transform.position = new Vector3(0, 3f, 0);
            rigidbody.velocity = Vector3.zero;
        }
        //Activates the ShockWave effect if BallSpeed is nearing 0 and after a little time
        speed = rigidbody.velocity.magnitude;

        if (speed <= 0.05)
        {
            ImmobilisationTimer += Time.deltaTime;
        }
        else
        {
            ImmobilisationTimer = 0;
            shockwave.IsAlive(false);
        }

        if (ImmobilisationTimer >= 5)
        {
            shockwave.Play();
            ImmobilisationTimer = 0;
        }

    }
    

    //On collision make a Noise
    private void OnCollisionEnter(Collision collision)
    {
        AudioManager.instance.ChangeVolume("BilleChocBois", GetComponent<Rigidbody>().velocity.magnitude / 2);
        AudioManager.instance.Play("BilleChocBois");
        
        // touch goal, change level
        if (collision.collider.CompareTag("GoalBG")|| collision.collider.CompareTag("GoalRY"))
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().ResetInertiaTensor();
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            AudioManager.instance.Play("PointGagne");
            GameManager.Instance.PointGain(collision.collider.tag);
            gameObject.SetActive(false);
        }

        Debug.Log("element touch "+collision.gameObject);
    }
}