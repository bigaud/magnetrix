﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


/*
 * Manage interactions in the menu
 */
public class MenuManager: MonoBehaviour
{
    // The logo animation
    public Animator animfond;

    // List of all button in menu
    public Button[] btsMenu, btsWorld, btsCredits;

    /*
     * Load the default world (Labyrinth) and begin the game
     */
    public void StartLevelQuick(string world)
    {
        BtsToSet("menu", false);
        AudioManager.instance.Play("MenuBouton");
        SceneManager.LoadScene(world);
    }

    /*
     * Select a world to load and begin the game
     */
    public void StartLevelWorldSelec(string world)
    {
        BtsToSet("world", false);
        AudioManager.instance.Play("MenuBouton");
        SceneManager.LoadScene(world);
    }

    /*
     * Show the world selection panel
     */
    public void SwitchMenuWorld(GameObject panel)
    {
        BtsToSet("menu", false);
        BtsToSet("world", true);
        panel.SetActive(true);
        AudioManager.instance.Play("MenuBouton");
    }

    /*
     * Show the credit panel
     */
    public void SwitchMenuCredits(GameObject panel)
    {
        BtsToSet("menu", false);
        BtsToSet("credits", true);
        panel.SetActive(true);
        AudioManager.instance.Play("MenuBouton");
    }

    /*
     * Quit the world selection panel and back to the menu
     */
    public void SwitchWorldMenu(GameObject panel)
    {
        BtsToSet("menu", true);
        BtsToSet("world", false);
        panel.SetActive(true);
        AudioManager.instance.Play("MenuBouton");
    }

    /*
     * Quit the credits panel and back to the menu
     */
    public void SwitchCreditsMenu(GameObject panel)
    {
        BtsToSet("menu", true);
        BtsToSet("credits", false);
        panel.SetActive(true);
        AudioManager.instance.Play("MenuBouton");
    }

    /*
     * Disable a panel
     */
    public void DesActivePanel(GameObject panel)
    {
        panel.SetActive(false);
    }

    /*
     * Quit the application
     */
    public void DoQuit()
    {
        BtsToSet("menu", false);
        AudioManager.instance.Play("MenuBouton");
        Application.Quit();
    }

    /*
     * Change the logo animation
     */
    public void ChangeAnim(bool returnpanel)
    {
        animfond.SetBool("isPanel", returnpanel);
    }

    /*
     * Set the world to load
     */
    public void ChangeWorld(int world)
    {
        GlobalVariables.gVar.currentWorldIndex = world;
    }

    public void Start()
    {
        AudioManager.instance.ResetSound();
        AudioManager.instance.Play("Mus_Menu");
    }

    /*
     * Enable or disable buttons incumbent a canvas
     */
    public void BtsToSet(string canvas, bool isActive)
    {
        switch (canvas)
        {
            case ("menu"):
                SetBts(btsMenu, isActive);
                break;
            case ("world"):
                SetBts(btsWorld, isActive);
                break;
            case ("credits"):
                SetBts(btsCredits, isActive);
                break;
            default:
                SetBts(btsMenu, isActive);
                break;
        }
    }

    /*
     * Disable or enable each button of the array
     */
    private void SetBts(Button[] btsSet, bool isActive)
    {
        foreach (Button button in btsSet)
        {
            button.enabled = isActive;
        }
    }
}
