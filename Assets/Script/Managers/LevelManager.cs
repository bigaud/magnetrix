﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
* Manage levels loading and unloading
*/
public class LevelManager : MonoBehaviour
{
    // Contain all world data scriptable object
    // A world data contains all levels names to load
    public WorldData[] worldDatas;

    // Enable the system to load level randomly
    // If randomLevels is disable, the game will load levels in the order defined by the world data list
    public bool randomLevels = false;

    // Property to access the current loaded world index
    public int CurrentWorldIndex => m_currentWorldIndex;
    
    // The current world loaded
    private WorldData m_currentWorld;

    // Contains all level data
    // Level data contains the scene to load, the root gameobject and the scene name
    private List<LevelData> m_currentWorldLevels;
    
    // The current level playing
    private LevelData m_currentLevel;

    // Count the number of loaded scene
    private int m_nbScenePreload = 0;

    // The current loaded world index
    private int m_currentWorldIndex;
    

    /*
     * Set the world to load
     * index is the world index
     * This function is used when we know which world to load
     */
    public void SetCurrentWorld(int index)
    {
        // We ensure that the index exist in the array
        if (index < worldDatas.Length)
        {
            m_currentWorldIndex = index;
            m_currentWorld = worldDatas[index];
        }
      
    }

    /*
     * Set the world to load
     * If randomLevels is true, we load the world randomly
     * This function is used if we don't want to load a special world
     */
    public void SetWorld()
    {
        if (randomLevels)
            m_currentWorldIndex = Random.Range(0, worldDatas.Length);
        else
            m_currentWorldIndex = 0;
        
        m_currentWorld = worldDatas[m_currentWorldIndex];
    }
    
    /*
     * Return true if players finished all levels 
     */
    public bool WorldIsFinished()
    {
        return m_currentWorldLevels.Count <= 0;
    }

    /*
     * Clear correctly all scene by unloading all of them
     * We don't use this function yet because when we reboot the game, we load without the Additive Mode
     */
    public void ClearLoadedScenes()
    {
        foreach (string levelName in m_currentWorld.levelNames)
        {
            StartCoroutine(CO_UnloadScene(levelName));
        }
    }

    /*
     * Callback when all scenes are unloaded
     */
    public void OnAllScenesUnloaded()
    {
        SceneManager.LoadScene(2);
    }

    /*
     * Load additively all levels scenes 
     */
    public void InitWorldLevels()
    {
        // We set the current level to default to avoid some undefined error
        m_currentLevel = new LevelData("",new Scene(), null);
        
        // We load all scenes additively
        // Because of doing that, we need to hide the rooted gameobject to hide the whole scene
        m_currentWorldLevels = new List<LevelData>();
        foreach (string levelName in m_currentWorld.levelNames)
        {
            StartCoroutine(CO_PreloadScene(levelName));
        }
    }

    /*
     * Load the next level
     */
    public void LoadNextLevel()
    {
        // We check if we have other levels to play
        if (m_currentWorldLevels.Count > 0)
        {
            // When the level is ended, we hide the rooted gameobject and remove it from the list
            // We need to check if the currentLevel have a scene name because
            // when this function is called for the first time, the currentLevel is set to default and so it doesn't exist in the list
            if (m_currentLevel.sceneName != string.Empty)
            {
                m_currentWorldLevels.Remove(m_currentLevel);
                m_currentLevel.root.SetActive(false);
            }
            
            if (randomLevels)
            {
                // We generate a random index to load level randomly
                int randomIndex = Random.Range(0, m_currentWorldLevels.Count);
                m_currentLevel = m_currentWorldLevels[randomIndex];
            }
            else
            {
                // We remove each already played level from the list. So we just need to take the first level in the list
                m_currentLevel = m_currentWorldLevels[0];
            }

            // We need to active the new scene to use the good lightmapping
            SceneManager.SetActiveScene(m_currentLevel.scene);
            m_currentLevel.root.SetActive(true);
        }
    }

    /*
     * Load the selected scene asynchronously
     */
    IEnumerator CO_PreloadScene(string sceneName)
    {
        AsyncOperation ao;
        ao = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

        while (!ao.isDone)
        {
            yield return null;
        }
        
        // We get the current scene loaded
        Scene scene = SceneManager.GetSceneByName(sceneName);
           
        // We save the data of this current scene
        LevelData levelData = new LevelData(sceneName, scene, scene.GetRootGameObjects()[0]);
           
        // We hide the rooted gameobject to hide the whole scene
        levelData.root.SetActive(false);
        
        m_currentWorldLevels.Add(levelData);

        m_nbScenePreload++;
        
        // We load all scene, so we are ready to begin the game and select the level to play
        if(m_nbScenePreload >= m_currentWorld.levelNames.Count)
            LoadNextLevel();
    }
    
    /*
     * Unload the selected scene asynchronously
     */
    IEnumerator CO_UnloadScene(string sceneName)
    {
        AsyncOperation ao;
        ao = SceneManager.UnloadSceneAsync(sceneName);

        while (!ao.isDone)
        {
            yield return null;
        }

        m_nbScenePreload--;
        
        if(m_nbScenePreload <= 0)
            OnAllScenesUnloaded();
    }
}
