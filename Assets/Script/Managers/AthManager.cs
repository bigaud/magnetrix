﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/*
 * Manage all UI
 */
public class AthManager : MonoBehaviour
{
    // Contains the "ready" text for each player
    public List<TextMeshProUGUI> playerStateFields;
    
    // This panel contains all ready state for each player
    public GameObject playersStatePanel;
    
    // All panels linked to victory or lose state
    public GameObject victoryStatePanel,victoryRed,VictoryBlue,loseRed,loseBlue;
    
    // Panel showed during pause
    public GameObject pausePanel;
    
    // Show advices and how to play the game
    public GameObject helpPanel;
    
    // Teams score in the UI
    public TextMeshProUGUI equipeRYScoreFields, equipeBGScoreFields;
    
    // Victory text for each team
    public TextMeshProUGUI victoryRYScoreFields, victoryBGScoreFields;

    // Informs players which team win 
    public TextMeshProUGUI infoGamePoint;
    
    // Show action that player needs to do
    public GameObject InfoImage;
    
    // Effect when one player plays a king
    public GameObject AnimKing;
    
    // VFX when players score a point
    public ParticleSystem gainPointVFX;
    
    // VFX when one team wins
    public ParticleSystem VictoryVFX;

    // Attach a the ready text to a player
    private Dictionary<PlayerBehavior, TextMeshProUGUI> m_playerFieldsLinks = new Dictionary<PlayerBehavior, TextMeshProUGUI>();

    /*
     * Attach each ready text to a player
     * We use an index to find which ready text corresponds to which player
    */
    public void SynchronisePlayersWithStateFields(PlayerBehavior player, int index)
    {
        m_playerFieldsLinks.Add(player, playerStateFields[index]);
    }

    /*
     * Update the ready text and color
     */
    public void UpdatePlayerStateFields(PlayerBehavior player, string newField, Color color)
    {
        if (m_playerFieldsLinks.ContainsKey(player))
        {
            m_playerFieldsLinks[player].text = newField;
            m_playerFieldsLinks[player].color = color;
            UpdateScoreFields();
        }
    }

    /*
     * Show or hide ready text 
     */
    public void ActivePlayerStateFields(PlayerBehavior player, bool isActive)
    {
        if (m_playerFieldsLinks.ContainsKey(player))
        {
            m_playerFieldsLinks[player].gameObject.SetActive(isActive);
        }
    }

    /*
     * Update teams scores
     */
    public void UpdateScoreFields()
    {
        equipeRYScoreFields.text = GameManager.Instance.scoreTeamRed + "";
        equipeBGScoreFields.text = GameManager.Instance.scoreTeamBlue + "";
    }

    /*
     * Determine which team win and update the victory panel
     */
    public void UpdateVictoryStateFields(int scoreRY, int scoreBG)
    {
        if (scoreBG>scoreRY) // Blue team win
        {
            VictoryBlue.SetActive(true);
            loseRed.SetActive(true);
            ParticleSystem.MainModule settings = VictoryVFX.main;
            settings.startColor = new ParticleSystem.MinMaxGradient(Color.blue);
        }
        else // Red team win
        {
            victoryRed.SetActive(true);
            loseBlue.SetActive(true);
            ParticleSystem.MainModule settings = VictoryVFX.main;
            settings.startColor = new ParticleSystem.MinMaxGradient(Color.red);
        }
        
        // We set the score for each team in the UI
        victoryRYScoreFields.text = GameManager.Instance.scoreTeamRed + "";
        victoryBGScoreFields.text = GameManager.Instance.scoreTeamBlue + "";
        ActivateObject(VictoryVFX.gameObject, true);
    }

    /*
     * Show or hide panel who give info about which player is ready or not
     */
    public void ShowPlayersStatePanel(bool show)
    {
        playersStatePanel.SetActive(show);
    }

    /*
     * Show or hide victory panel
     */
    public void ShowVictoryStatePanel(bool show)
    {
        victoryStatePanel.SetActive(show);
    }
    
    /*
     * Show or hide pause panel
     */
    public void ShowPausePanel(bool show)
    {
        pausePanel.SetActive(show);
        AudioManager.instance.Play("bouton");
    }

    /*
     * Show or hide help panel
     */
    public void ShowHelpPanel(bool show)
    {
        helpPanel.SetActive(show);
        AudioManager.instance.Play("bouton");
    }

    /*
     * Set which team win in the UI
     */
    public void UpdatePanelInfoTeamPoint(string TeamPoint)
    {
        infoGamePoint.text = TeamPoint;
    }
    /*
     * Show or hide the ingame tips
     */
    public void ActivePanelInfoTeamPoint(bool isActive)
    {
        infoGamePoint.gameObject.SetActive(isActive);
    }

    /*
     * Disable an object
     * Not really useful we can refact this
     */
    public void DeActivateObject(GameObject objetActivate)
    {
        AudioManager.instance.Play("bouton");
        objetActivate.SetActive(false);
    }

    /*
     * Show or hide and change the tips animation
     */
    public void SetAnimationInfo(bool isActive,bool isShuffle)
    {
        if (isActive!=InfoImage.activeSelf)
        {
            InfoImage.SetActive(isActive);
        }

        InfoImage.GetComponent<Animator>().SetBool("isShuffle", isShuffle);
    }

    /*
     * Show the King Activate Effect
     */
    public void ActiveKing()
    {
        AnimKing.SetActive(true);
    }

    /*
     * Active an object
     * Not really useful we can refact this
     */
    public void ActivateObject(GameObject activate, bool isActive)
    {
        activate.SetActive(isActive);
    }
}
