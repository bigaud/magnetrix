﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    ///<summary>
    /// This script is used for managepause
    /// </summary>
    /// 

    public int pageHelpIndex;

    public GameObject panelActive;
    public GameObject[] helpPanel;

    public void ShowPanel(int pagePlus)
    {
        AudioManager.instance.Play("bouton");
        pageHelpIndex += pagePlus;
        
        if (pageHelpIndex > helpPanel.Length-1)
        {
            pageHelpIndex = 0;
        }
        else if(pageHelpIndex < 0)
        {
            pageHelpIndex = helpPanel.Length - 1;
        }
        
        panelActive.SetActive(false);
        panelActive = helpPanel[pageHelpIndex];
        panelActive.SetActive(true);
    }
}
