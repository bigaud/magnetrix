﻿using UnityEngine.Audio;
using System;
using UnityEngine;

/*
 * Manage audio 
 */
public class AudioManager : MonoBehaviour
{
    // Contains all sounds
    public Sound[] sounds;

    // Singleton to access everywhere
    public static AudioManager instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        // For each sound, we add and setup a audio source
        foreach (Sound s in sounds)
        {
            // on ajoute les variables du sound
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }
    
    /*
     * Play the selected sound
     */
    public void Play(string name)
    {
        // We search the corresponding sound
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.Play();
    }

    /*
     * Stop the selected sound
     */
    public void Stop(string name)
    {
        // We search the corresponding sound
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.Stop();
    }

    /*
     * Stop all sound
     */
    public void ResetSound()
    {
        foreach (Sound s in sounds)
        {
            s.source.Stop();
        }
    }

    /*
     * Change the volume of a sound
     */
    public void ChangeVolume(string name, float amount)
    {
        // We search the corresponding sound
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.volume = amount;
    }
}
