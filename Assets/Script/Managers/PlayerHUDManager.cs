﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/*
 * A handler to manage each player interface
 */
public class PlayerHUDManager : MonoBehaviour
{
    // Feedback for the cooldown
    public Image cooldown;
    
    // Cooldown text
    public Text cooldownField;
    
    // List of card player feedback during the second turn
    public Transform cardPlayedT2;
    
    // List of card player feedback during the first turn
    public Transform cardPlayedT1;
    
    // Define for each state the position of the button
    public Transform PositionDraw, PositionPlay;
    
    // The button to end turn/set ready
    public Button readyButton;
    
    // The text in the button
    public TextMeshProUGUI readyButtonField;
    
    // Pion indication
    public Image pionIncitation;
    
    // Card indication
    public Image IndicationImage;

    private bool m_readyButtonContainListener = false;

    /*
     * Change the ready button text
     */
    public void SetReadyButtonText(string text)
    {
        readyButtonField.text = text;
    }

    /*
     * Show or hide pion indication
     */
    public void SetIncitationButton(bool isActive)
    {
        pionIncitation.gameObject.SetActive(isActive);
    }

    /*
     * Change the button position incumbent 
     */
    public void MoveButton(bool isPlay)
    {
        if (isPlay)
        {
            readyButton.transform.position = PositionPlay.position;
            readyButton.transform.rotation = PositionPlay.rotation;
        }
        else
        {
            readyButton.transform.position = PositionDraw.position;
            readyButton.transform.rotation = PositionDraw.rotation;
        }
    }

    public void SetReadyButtonEvents(UnityEvent evt)
    {
        //We add a condition to avoid to add the same event on the same button
        if (!m_readyButtonContainListener)
        {
            readyButton.onClick.AddListener(evt.Invoke);
            m_readyButtonContainListener = true;
        }

    }

    // Remove all events on the ready button
    public void ClearReadyButtonEvents()
    {
        readyButton.onClick.RemoveAllListeners();
        m_readyButtonContainListener = false;
    }

    //change color button player
    public void SetReadyButtonColor(Color32 color)
    {
        readyButton.GetComponent<Image>().color = color;
    }

    public void SetCooldownText(string cooldown)
    {
        cooldownField.text = cooldown;
    }

    // setactive yellow sphere one by one
    public void ShowCardPlayedCount(int turn, int nbCard, string nomCard)
    {
        if (turn == 1)
        {
            cardPlayedT1.GetChild(nbCard - 1).gameObject.SetActive(true);
            cardPlayedT1.GetChild(nbCard - 1).GetComponentInChildren<Text>().text = nomCard;
        }
        else
        {
            cardPlayedT2.GetChild(nbCard - 1).gameObject.SetActive(true);
            cardPlayedT2.GetChild(nbCard - 1).GetComponentInChildren<Text>().text = nomCard;
        }
    }

    // setactive yellow sphere one by one
    public void ClearCardCount()
    {
        foreach (Transform child in cardPlayedT1)
        {
            child.gameObject.SetActive(false);
        }
        
        foreach (Transform child in cardPlayedT2)
        {
            child.gameObject.SetActive(false);
        }
    }

    public void ActiveReady(bool isActive)
    {
        readyButtonField.gameObject.SetActive(isActive);
    }

    public void ActiveIndicationCarte(bool isActive)
    {
        IndicationImage.gameObject.SetActive(isActive);
    }

    IEnumerator ShowForce()
    {
        cooldownField.text = "MAX";
        yield return new WaitForSeconds(1f);
        cooldownField.text = "30";
    }
}
