﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAnimFinished : MonoBehaviour
{
    public Animation animationOne;

    public void Desactivate()
    {
        gameObject.SetActive(false);
    }
}
