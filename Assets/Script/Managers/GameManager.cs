﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/*
 * Manage game loop and core gameplay 
 */
public class GameManager : MonoBehaviour
{
    // A simple singleton to get access to the gamemanager everywhere
    public static GameManager Instance { private set; get; }

    [Header("Links")]
    // Manage all UI 
    public AthManager athManager;

    [Header("Data")]
    // List of all players
    public PlayerBehavior[] players;
    
    // List of all cards
    public CardData[] allPlayableCards;
    
    // All interactibles elements
    public List<IInteractableElement> interactable= new List<IInteractableElement>();
    
    [Header("Parameters")]
    public bool drawState;

    // Count the number of turn played
    public int countTurn;
    
    // Determine when we reset cards played
    public int turnChange = 2;
    
    // Score of the blue team
    public int scoreTeamBlue;
    
    // Score of the red team
    public int scoreTeamRed;
    
    // Define the score to reach to win the game
    public int scoreMax ;
    
    // It is the first turn ?
    public bool isLevelBeginning;
    
    // Some timers to delay some transitions
    public float timeWaitState, timeUpdateScore;
    
    // The main scene of the current loaded world
    public string currentWorldMainSceneLoaded;

    // Ball velocities infos
    public Vector3 ballAngularVelocity, ballVelocity;
    
    // Property of the current game state 
    public IGameState State
    {
        set { m_state = value; }
        get { return m_state; }
    }

    // Reference to the level Manager
    private LevelManager m_levelManager;

    // The Game state
    private IGameState m_state;
    
    // List of ready player (for the draw phase)
    private List<PlayerBehavior> m_playersReady = new List<PlayerBehavior>();

    private void Awake()
    {
        Instance = this;
        m_levelManager = GetComponent<LevelManager>();
        
        // We get the main loaded scene
        currentWorldMainSceneLoaded = SceneManager.GetActiveScene().name;
        
        // Manage when players choose or not the world
        if (GlobalVariables.gVar.currentWorldIndex == -1)
        {
            m_levelManager.SetWorld();
        }
        else
        {
            m_levelManager.SetCurrentWorld(GlobalVariables.gVar.currentWorldIndex);
        }
        
        m_levelManager.InitWorldLevels();
        GlobalVariables.gVar.ResetVariables();
        
        // We check witch music we need to launch
        switch (currentWorldMainSceneLoaded)
        {
            case ("GameLeVrai"):
                AudioManager.instance.Play("Mus_Laby");
                AudioManager.instance.Play("Mus_Laby_Deep");
                AudioManager.instance.ChangeVolume("Mus_Laby", 0);
                break;
            case ("GamePachinko"):
                AudioManager.instance.Play("Mus_Pachi");
                AudioManager.instance.Play("Mus_Pachi_Deep");
                AudioManager.instance.ChangeVolume("Mus_Pachi", 0);
                break;
            default:
                AudioManager.instance.Play("Mus_Laby");
                AudioManager.instance.Play("Mus_Laby_Deep");
                AudioManager.instance.ChangeVolume("Mus_Laby", 0);
                break;
        }
        AudioManager.instance.Stop("Mus_Menu");

        
    }

    void Start()
    {
        // For all players, we connect the player to his corresponding UI 
        for(int i = 0; i < players.Length; ++i) 
        {
            athManager.SynchronisePlayersWithStateFields(players[i], i);
        }

        // We define the first state 
        m_state = GameStateBase.GAME_STATE_DRAW;
        m_state.OnEnter(this);
        drawState = true;
        
        AudioManager.instance.Stop("Mus_Victory");

        isLevelBeginning = true;
    }

    // the update is used for debug the game
    private void Update()
    {
        #region Debug
        if (Input.GetKeyDown(KeyCode.A))
            PointGain("GoalBG");

        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene("Game");
        #endregion
    }
    
    
    #region STATE FUNCTION
    /*
     * Set a player ready and manage his state
     */
    public void SetPlayerReady(PlayerBehavior player)
    {
        // We check if the player already pressed play
        if (m_playersReady.Contains(player))
        {
            // If yes, we remove the player from the ready state
            m_playersReady.Remove(player);
            
            // We switch back the player state to the draw phase
            player.State.ToState(player, PlayerStateBase.PLAYER_STATE_DRAW);
            
            // We update the interface
            player.ath.SetReadyButtonColor(new Color32(255,255,255,255));
            player.PlayerZone.color = new Color32(94, 94, 94, 255);
            athManager.UpdatePlayerStateFields(player, "NOT READY", Color.red);
        }
        else
        {
            // We add the player to the ready state
            m_playersReady.Add(player);
            
            // We switch the player state to a wait phase
            player.State.ToState(player, PlayerStateBase.PLAYER_STATE_WAIT);

            // We update the interface
            player.ath.SetReadyButtonColor(new Color32(50, 50, 50, 128));
            player.PlayerZone.color = new Color32(255, 255, 255, 255);
            athManager.UpdatePlayerStateFields(player, "READY", Color.green);
        }

        if (m_playersReady.Count == players.Length) // All players are ready
        {
            NfcAsync.StartTagDetection();
            // If all player are ready, we can switch to the play state
            m_state.ToState(this, GameStateBase.GAME_STATE_PLAY);
            drawState = false;
        }
        
    }

    /*
     * Clear all ready players
     */
    public void ResetPlayerReady()
    {
        m_playersReady.Clear();
    }

    /*
     * Callback when one player end his turn
     */
    public void OnPlayerEndTurn(PlayerBehavior player)
    {
        m_playersReady.Remove(player);
    }

    /*
     * Check if all players have ended turn
     */
    public bool AllPlayerEndedTurn()
    {
        return m_playersReady.Count == 0;
    }


    #endregion
    

    /*
     * Return the card data 
     */
    public CardData GetCard(string cardName)
    {
        int i = 0;
        bool find = false;

        while (!find && i < allPlayableCards.Length)
        {
            if (allPlayableCards[i].cardName == cardName)
            {
                find = true;
            }
            ++i;
        }

        if (!find)
        {
            return default;
        }
        
        return allPlayableCards[i - 1];
    }

    /*
     * Call when a player scores a point
     */
    public void PointGain (string team)
    {
        // We will pass to a next level, so we need to reset some properties
        ResetPlayerCards();
        ResetCoolDown();
        ResetPlayerReady();
        interactable.Clear();
        
        
        if (team == "GoalBG") // If Team blue score a point
        {
            athManager.UpdatePanelInfoTeamPoint("Blues Have Scored !");
            ParticleSystem.MainModule settings = athManager.gainPointVFX.main;
            settings.startColor = new ParticleSystem.MinMaxGradient(Color.blue);
            scoreTeamBlue++;
        }
        else if (team == "GoalRY") // If Team red score a point
        {
            ParticleSystem.MainModule settings = athManager.gainPointVFX.main;
            settings.startColor = new ParticleSystem.MinMaxGradient(Color.red);
            athManager.UpdatePanelInfoTeamPoint("Reds Have Scored !");
            scoreTeamRed++;
        }
        
        athManager.ActivateObject(athManager.gainPointVFX.gameObject, true);
        
        // Check if one team win the game
        if (scoreTeamBlue >= scoreMax || scoreTeamRed >= scoreMax)
        {
            m_state.ToState(this, GameStateBase.GAME_STATE_VICTORY);
        }
        else
        {
            m_state.ToState(this, GameStateBase.GAME_STATE_WAIT);
        }
    }

    /*
     * Callback when the level is ended
     */
    public void OnLevelEnded()
    {
        //If remains level, load a random level from the List
        if (!m_levelManager.WorldIsFinished())
        {
            m_levelManager.LoadNextLevel();
            
            // We reset the ball
            BallBehavior.Instance.gameObject.SetActive(true);
            BallBehavior.Instance.transform.position = new Vector3(0, 4f, 0);
            
            m_state.ToState(this, GameStateBase.GAME_STATE_DRAW);
            drawState = true;
            
            foreach (PlayerBehavior player in players)
            {
                player.ResetForce();
                player.lineRenderer.gameObject.SetActive(false);
                player.turn = 1;
                player.ath.ClearCardCount();
            }
        }
        //Else come back to menu
        else
        {
            ReturnMenu();
            m_state.ToState(this, GameStateBase.GAME_STATE_DRAW);
        }
    }
    

    /*
     * Reset all cards played for each player
     */
    public void ResetPlayerCards()
    {
        foreach (PlayerBehavior player in players)
        {
            player.cardsPlayed.Clear();
            countTurn = 0;
        }
    }

    /*
     * For each player, reset play cooldown
     */
    public void ResetCoolDown()
    {
        foreach (PlayerBehavior player in players)
        {
            player.coolDownRemaining = 0;
            player.ath.cooldown.fillAmount = 1;
        }
    }

    /*
     * Reload the current world
     */
    public void ReloadGame()
    {
        GlobalVariables.gVar.currentWorldIndex = m_levelManager.CurrentWorldIndex;
        SceneManager.LoadScene(currentWorldMainSceneLoaded);
    }

    /*
     * Load the menu
     */
    public void ReturnMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    /*
     * Active the pause
     * During pause, the game is not freeze (we just stop the ball) but we stop card detection
     */
    public void ActivatePause()
    {
        NfcAsync.StopTagDetection();
        
        // We don't want to freeze the whole game (with Time.timescale = 0)
        // So we save ball velocities and stop it
        ballAngularVelocity = BallBehavior.Instance.GetComponent<Rigidbody>().angularVelocity;
        ballVelocity = BallBehavior.Instance.GetComponent<Rigidbody>().velocity;
        BallBehavior.Instance.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        BallBehavior.Instance.GetComponent<Rigidbody>().velocity = Vector3.zero;

        //Changes the music when paused
        AudioManager.instance.ChangeVolume("Mus_Laby", 0);
        AudioManager.instance.ChangeVolume("Mus_Laby_Deep", 0.85f);
        AudioManager.instance.ChangeVolume("Mus_Pachi", 0);
        AudioManager.instance.ChangeVolume("Mus_Pachi_Deep", 0.85f);

    }

    /*
     * Stop the pause 
     */
    public void DeActivatePause()
    {
        Debug.Log("UNpause");
        //when pause is activate, you must take the system into account
        NfcAsync.StartTagDetection();
        
        // We re-add velocities to the ball
        // She will take over it traveling 
        BallBehavior.Instance.GetComponent<Rigidbody>().angularVelocity=ballAngularVelocity;
        BallBehavior.Instance.GetComponent<Rigidbody>().velocity= ballVelocity;

        if (drawState == false)
        {
            //Changes the music when unpaused
            AudioManager.instance.ChangeVolume("Mus_Laby", 0.85f);
            AudioManager.instance.ChangeVolume("Mus_Laby_Deep", 0);
            AudioManager.instance.ChangeVolume("Mus_Pachi", 0.85f);
            AudioManager.instance.ChangeVolume("Mus_Pachi_Deep", 0);
        }
    }

    /*
     * Close the application
     */
    public void Quit()
    {
        Application.Quit();
    }
}

