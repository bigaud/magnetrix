﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moulin : MonoBehaviour, IInteractableElement
{
    public float rotateSpeed;
    public float turnGo=1;

    public Material mBlue, mRed;
    
    private void OnEnable()
    {
        AttachToGameManager();
        if (turnGo>=1)
        {
            gameObject.GetComponent<Renderer>().material = mRed;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material = mBlue;
        }
    }

    public void OnDisable()
    {
        GameManager.Instance.interactable.Remove(this);
    }

    public void AttachToGameManager()
    {
        GameManager.Instance.interactable.Add(this);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(Vector3.forward , rotateSpeed*turnGo);
    }

    public void Trigger()
    {
        if (turnGo>0)
        {
            turnGo = -1;
            gameObject.GetComponent<Renderer>().material = mBlue;
        }
        else
        {
            turnGo = +1;
            gameObject.GetComponent<Renderer>().material = mRed;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("balle");
    }
}
