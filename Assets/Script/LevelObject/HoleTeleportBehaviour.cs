﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HoleTeleportBehaviour : MonoBehaviour
{
    /// <summary>
    /// This script move the marble in one place to another -------------------
    ///  
    /// script creator : Pierre Bigaud
    ///  
    /// This script is for hole transportation in the labyrinth arena.
    /// It takes the marble, makes her look like falling in the hole, and spawn here in another place
    /// 
    /// Variable : the marble, transform places to TP, places to fall
    /// </summary>
    /// 

    // variable ball, to move her and simulates falling
    private GameObject bBBalleToMove;
    private GlobalVariables varGlob;

    public Sprite[] spriteEntree, spriteSortie;

    public float vitesseAnime=0.1f,timeNonAnime=3f;

    private int indexAnime;

    // position where the hole is located
    private GameObject tranHole;

    // position where the marble falls
    private GameObject tranFall;

    // boolean for when the ball dropped
    private bool boolFalling,isInfall;

    // we take the variable transport and hole, and globalvariables.
    private void Awake()
    {
        tranHole= gameObject.transform.GetChild(0).gameObject;
        tranFall= gameObject.transform.GetChild(1).gameObject;
        InvokeRepeating("InvokeSpriteAnimation", timeNonAnime,vitesseAnime);
        varGlob = GameObject.FindGameObjectWithTag("Variables").GetComponent<GlobalVariables>();
    }

    private void OnTriggerEnter(Collider other)
    {
        // detect tag marble
        if (other.tag=="Ball" && isInfall == false)
        {
            Debug.Log("oiseau");
            //First, we diminish the size of the ball and we move her toward the goal
            other.GetComponent<Rigidbody>().isKinematic = true;
            other.GetComponent<Rigidbody>().velocity = Vector3.zero;
            GoToHole(other.gameObject);
            isInfall = true;
        }
    }

    // method to teleport the marble
    private void TeleportMarble()
    {
        bBBalleToMove.transform.position = tranFall.transform.position + Vector3.up;
        bBBalleToMove.GetComponent<Rigidbody>().isKinematic = false;
        bBBalleToMove.transform.localScale = new Vector3(varGlob.b_size, varGlob.b_size, varGlob.b_size);
        bBBalleToMove = null;
        isInfall = false;
        Debug.Log("teleportmarble");
    }

    //method to fall the marble (we used DOtween for animation)
    private void GoToHole(GameObject marble)
    {
        AudioManager.instance.Play("TrouTp");
        bBBalleToMove = marble;
        bBBalleToMove.GetComponent<Rigidbody>().isKinematic = true;
        bBBalleToMove.transform.DOScale(Vector3.zero, 0.45f);
        bBBalleToMove.transform.DOMove(tranHole.transform.position, .49f);
        Invoke("TeleportMarble", 0.61f);
        Debug.Log("passez trou tp");
    }

    public void InvokeSpriteAnimation()
    {
        indexAnime++;
        if (indexAnime>= spriteEntree.Length || indexAnime >= spriteSortie.Length)
        {
            indexAnime = 0;
        }
        tranHole.GetComponent<SpriteRenderer>().sprite = spriteEntree[indexAnime];
        tranFall.GetComponent<SpriteRenderer>().sprite = spriteSortie[indexAnime];
    }
}
