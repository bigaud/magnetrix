﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurSwitch : MonoBehaviour, IInteractableElement
{
    public bool isActive;
    public GameManager boud;
    public Collider collisionbox;

    // Start is called before the first frame update
    public void OnEnable()
    {
        AttachToGameManager();
        if (isActive)
        {
            isActive = false;
            GetComponent<Animator>().SetTrigger("on");
            collisionbox.isTrigger = false;
        }
        else
        {
            isActive = true;
            GetComponent<Animator>().SetTrigger("off");
            collisionbox.isTrigger = true;
        }
    }

    public void OnDisable()
    {
        GameManager.Instance.interactable.Remove(this);
    }

    public void AttachToGameManager()
    {
        GameManager.Instance.interactable.Add(this);
    }

    // Update is called once per frame
    public void Trigger()
    {
        if (isActive)
        {
            isActive = false;
            GetComponent<Animator>().SetTrigger("on");
            collisionbox.isTrigger = false;
        }
        else
        {
            isActive = true;
            GetComponent<Animator>().SetTrigger("off");
            collisionbox.isTrigger = true;
        }
    }
}
