﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedTexture : MonoBehaviour
{
    public float speed = 0.5f;
    public float duration = 2.0f;
    float alpha = 0;
    float offset;
    Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update () 
    {
        //creates an uv offset so the texture moves at every update
        Vector2 textureOffset = new Vector2(Time.time*speed,0);
        rend.material.mainTextureOffset = textureOffset;
        LerpAlpha();
    }

    void LerpAlpha()
    {
        //a lerp for the opacity
        float lerp = Mathf.PingPong(Time.time, duration)/duration;
        alpha = Mathf.Lerp(0.4f, 1.0f, lerp);
        //updates the material color
        rend.material.color = new Color(1.0f, 1.0f, 1.0f, alpha);
    }
}
