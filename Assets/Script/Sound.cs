﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound
{
    /// <summary>
    /// cette classe sert uniquement a contenir les informations de sons
    /// </summary>

    public string name;

    public AudioClip clip;

    [Range(0, 1)]
    public float volume;
    [Range(.1f, 3)]
    public float pitch;

    public bool loop;

    [HideInInspector]
    public AudioSource source;
}
