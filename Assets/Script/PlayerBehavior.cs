﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerBehavior : MonoBehaviour
{
    /// <summary>
    /// This script is the player manager of the game, each player has one
    /// </summary>
    /// 
    [Header("References")]
    public PlayerHUDManager ath;
    // player position
    public Transform playerPos;
    public LineRenderer lineRenderer;
    public RectTransform buttonsTransform;
    public GameObject circle;
    public GameObject playedCard;

    [Header("Parameters")]
    
    public LayerMask raycastHits;
    public Vector3 offset;

    //force build up
    public int force;

    // To clear card played in UI
    public bool clearCard = false;

    //cooldown for playing card
    public float cooldownTime;
    public float coolDownRemaining, coolDownMultiply;

    //cardCount who determine the time you can play
    public int countCard;

    // Count the number of turn played
    public int turn = 1;

    //effect play
    public ParticleSystem[] ps;

    public List<CardData> cardsPlayed;

    //sprite to rend black if no more card
    public SpriteRenderer PlayerZone;
    
    //marble
    private GameObject m_ball;

    public bool pionplacer;

    public IPlayerState State
    {
        set { m_state = value;}
        get { return m_state; }
    }

    private GameManager m_gameManager;
    private IPlayerState m_state;
    private bool m_isReady;

    void Awake()
    {
        m_state = PlayerStateBase.PLAYER_STATE_WAIT;
    }

    void Start()
    {
        m_gameManager = GameManager.Instance;
        m_ball = FindObjectOfType<BallBehavior>().gameObject;
        Debug.Log("globalvariable"+ GlobalVariables.gVar.coolDownPlayer);
        coolDownMultiply = GlobalVariables.gVar.coolDownPlayer;
    }
    
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.P))
        {
            AddForce(1);
            if (ath.IndicationImage.gameObject.activeSelf)
            {
                ath.ActiveIndicationCarte(false);
            }
        }

        if (Input.GetKeyUp(KeyCode.O) && GameManager.Instance.players[1]==this)
        {
            Effect();
        }

        if (m_state != null)
            m_state.Update(this);
    }

    public void OnTouchScreen()
    {
        /// <summary>
        /// 
        /// Ontouchscreen displaythe line renderer
        /// 
        /// </summary>
        
        Vector3 target = m_ball.GetComponent<Rigidbody>().position;
        Vector3 start = playerPos.position;

        target.y =  m_ball.GetComponent<Rigidbody>().position.y;
        start.y = m_ball.GetComponent<Rigidbody>().position.y;

        lineRenderer.positionCount = 2;

        lineRenderer.SetPosition(0, start);
        lineRenderer.SetPosition(1, target);
    }
    
    public void DetectZoneTouch(Vector3 _pos, int count)
    {
        //Launch Raycast
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(_pos);
    
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, raycastHits))
        {
            //If same color
            if (hit.collider != null && hit.collider.GetComponent<PlayerCollider>().playerBehavior == this)
            {
                //Set player at Click
                playerPos.position = hit.point + offset;
                if (lineRenderer.gameObject.activeSelf==false)
                {
                    lineRenderer.gameObject.SetActive(true);
                    circle.SetActive(true);
                    pionplacer = true;
                }
                if (ath.pionIncitation.gameObject.activeSelf==true)
                {
                    ath.SetIncitationButton(false);
                    ath.ActiveIndicationCarte(true);
                    AudioManager.instance.Play("PoserPion1");
                }
            }
        }
    }

    /// <summary>
    /// add power and effect if card count
    /// </summary>
    /// <param name="power"></param>
    public void AddForce(int power)
    {
        force += power;
        if (force>=GlobalVariables.gVar.maxForce)
        {
            force = GlobalVariables.gVar.maxForce;
            ath.StartCoroutine("ShowForce");
        }
        else
        {
            ath.SetCooldownText(force.ToString());
            AudioManager.instance.Play("Card");
            if (!playedCard.GetComponent<Animator>().isActiveAndEnabled)
            {
                playedCard.GetComponent<Animator>().enabled = true;
                playedCard.GetComponent<Animator>().SetBool("CardPlayed", true);
            }
            //play vfx
            else
            {
                playedCard.GetComponent<Animator>().SetBool("CardPlayed", true);
            }
        }
    }

    public void ResetForce()
    {
        force = 0;
        ath.SetCooldownText(force.ToString());
    }

    // iscardgood search for the card you have and return if you have them or not 
    private bool AlreadyPlayed(CardData card)
    {
        if (cardsPlayed.Contains(card))
        {
            AudioManager.instance.Play("notPossible");
            return true;
        }
        else if (force>=30 && !card.isAnEffect)
        {
            ath.StartCoroutine("ShowForce");
            AudioManager.instance.Play("notPossible");
            return true;
        }
        return false;
    }

    // when a player plays a card (test if he can)
    public void PlayedCard(CardData cardToPlay)
    {
        if (!AlreadyPlayed(cardToPlay) && countCard < 4 && pionplacer)
        {
            //if the card is new, add it to the decks
            cardsPlayed.Add(cardToPlay);
            countCard++;
            if (cardToPlay.cardName==":")
            {
                ath.ShowCardPlayedCount(turn, countCard, "10");
            }
            else
            {
                ath.ShowCardPlayedCount(turn, countCard, cardToPlay.cardName);
            }
            

            if (ath.IndicationImage.gameObject.activeSelf == true)
            {
                ath.ActiveIndicationCarte(false);
            }
            cardToPlay.ApplyEffect(this);
        }
        else
        {
            AudioManager.instance.Play("notPossible");
            return;
        }
    }

    //On called, apply an Impulse
    public void Magnetize(int power)
    {
        if (m_state != PlayerStateBase.PLAYER_STATE_PLAY)
        {
            AudioManager.instance.Play("notPossible");
            return;
        }

        if (playerPos.localPosition == Vector3.zero)
        {
            AudioManager.instance.Play("notPossible");
            return;
        }

        if (force<=0)
        {
            AudioManager.instance.Play("notPossible");
            return;
        }

        //If cooldown passed, apply force, else return
        if (coolDownRemaining <= 0)
        {
            power = power * Mathf.FloorToInt(Mathf.Sqrt(force * 10) + force / 4);
            coolDownRemaining = force* coolDownMultiply;
            cooldownTime = force;
            ath.cooldown.fillAmount = 0;
            force = 0;
            ath.SetCooldownText("" + force);
            //forceTxt.text = force.ToString();

            if (countCard >= 4)
            {
                EndTurn();
            }
        }
        else
        {
            AudioManager.instance.Play("notPossible");
            return;
        }

        //Add Force
        Vector3 dir = (m_ball.transform.position - playerPos.position).normalized;
        Vector3 dir2 = new Vector3(dir.x, 0, dir.z);
        Debug.Log("Add Force = " + (dir2 * power));
        m_ball.GetComponent<Rigidbody>().AddForce(dir2 * power, ForceMode.Impulse);


        Vector3 pos = m_ball.GetComponent<Rigidbody>().position;
        //Apply visuals and sounds effects (pull)
        
        if (power < 0)
        {
            AudioManager.instance.Play("TirerBille");
            ps[0].transform.LookAt(playerPos.position);
            ps[0].transform.position = m_ball.transform.position;
            ps[0].transform.eulerAngles = new Vector3(0, ps[0].transform.eulerAngles.y - 90, 0);
            ps[0].Play();
        }
        if (power > 0)
        {
            ps[1].transform.LookAt(playerPos.position);
            ps[1].transform.position = m_ball.transform.position;
            ps[1].transform.eulerAngles = new Vector3(0, ps[1].transform.eulerAngles.y + 90, 0);
            ps[1].Play();
            AudioManager.instance.Play("PousserBille");
        }
    }
    //On called, apply an Effect on the map
    public void Effect()
    {
        AudioManager.instance.Play("MurSwitch");
        foreach (IInteractableElement ie in GameManager.Instance.interactable)
        {
            GameManager.Instance.athManager.ActiveKing();
            ie.Trigger();
        }
        if (countCard >= 4 && force<=0)
        {
            EndTurn();
        }
    }
    
    public void SetReady()
    {
        m_gameManager.SetPlayerReady(this);
        AudioManager.instance.Play("Pret");
    }
    
    /// <summary>
    ///  this function return the state of the player and end the state game play if every player have changed state
    /// </summary>
    public void EndTurn()
    {

        m_state.ToState(this, PlayerStateBase.PLAYER_STATE_WAIT);
        AudioManager.instance.Play("EndTurn");
        m_gameManager.OnPlayerEndTurn(this);
        
        turn++;
        if (turn > 2)
        {
            turn = 1;
            clearCard = true;
        }   


        if (m_gameManager.AllPlayerEndedTurn())
        {
            NfcAsync.StopTagDetection();
            m_gameManager.State.ToState(m_gameManager, GameStateBase.GAME_STATE_DRAW);;
        }
    }
}