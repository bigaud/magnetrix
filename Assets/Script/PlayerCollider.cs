﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    public PlayerBehavior playerBehavior;
    public float angle;

    private void Start()
    {
        playerBehavior = transform.parent.GetComponent<PlayerBehavior>();
    }
}
