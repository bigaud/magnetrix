﻿using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class TouchMove : MonoBehaviour
{
    public LayerMask raycastHits;
    public Vector3 offset;

    /// <summary>
    /// Touch move is a script who is used for pawn
    /// </summary>
    void Update()
    {
        // If we click on button, we do nothing to avoid change position
        if (EventSystem.current.currentSelectedGameObject != null)
            return;
        
        //If Click
        if (Input.GetMouseButtonDown(0))
            DetectZoneTouch(Input.mousePosition, 0);
        //If Touch
        if (Input.touchCount > 0)
            foreach (Touch touch in Input.touches)
                DetectZoneTouch(touch.position, touch.fingerId);
    }

    void DetectZoneTouch(Vector3 _pos, int count)
    {
        //Launch Raycast
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(_pos);

        if (Physics.Raycast(ray, out hit, raycastHits))
        {
            //If same color
            if (hit.transform.parent == transform.parent)
            {
                //Set player at Click
                transform.position = hit.point + offset;
                Sound s = Array.Find(AudioManager.instance.sounds, sound => sound.name == "PoserPion"+count);
                if (s != null)
                {
                    if (!s.source.isPlaying)
                    {
                        AudioManager.instance.Play("PoserPion"+count);
                    }
                }
            }
        }
    }
}