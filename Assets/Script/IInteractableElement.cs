﻿using UnityEngine;

public interface IInteractableElement
{
    void Trigger();
    void AttachToGameManager();
}